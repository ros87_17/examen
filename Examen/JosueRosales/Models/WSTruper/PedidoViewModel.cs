﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JosueRosales.Models.WSTruper
{
    public class PedidoViewModel
    {

        public int IDPedido { get; set; }
        public float TotalPedido { get; set; }
        public DateTime FechaPedido { get; set; }
        public string UserName { get; set; }

        public string NombrePedido { get; set; }
        public List<DetPedidoViewModel> DetallePedido { get; set; }
    }
}