﻿using JosueRosales.Models.DB;
using JosueRosales.Models.WSTruper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JosueRosales.Controllers
{
    public class PedidoController : BaseController
    {
        //Linea para enviar todos los Json Serializados



        [HttpGet]
        [Authorize(Roles = "ADMIN")]
        public Reply ObtenerProductos()
        {
            Reply oR = new Reply();
            oR.result = 0;
            try 
            {
                using (examenEntities db = new examenEntities())
                {
                    var lst = db.PRODUCTO_W.Where(d => d.EXISTENCIA > 0);
                    if (lst.Count() > 0)
                    {
                        oR.result = 1;
                        oR.data = lst.Select(producto => new ProductoViewModel()
                        {
                            SKU = producto.SKU,
                            Nombre = producto.NOMBRE,
                            Extistencia = (int)producto.EXISTENCIA,
                            Price = (float)producto.PRICE
                        }).ToList() ;
                    }
                    else 
                    {
                        oR.message = "No se encuentran datos de Productos";
                    }
                }
            }
            catch(Exception ex) 
            {
                oR.result = 0;
                oR.message = "Ocurrio un problema al obtener los productos intente mas tarde";
            }
            return oR;
        }
        
        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public Reply BuscarPedido([FromBody]PedidoViewModel model) 
        {
            Reply oR = new Reply();

            oR.result = 0;

            try
            {
                using (examenEntities db = new examenEntities())
                {
                    var lst = db.PEDIDOS_W.Where(d => d.ID== model.IDPedido);
                    var lstDet = db.DETALLE_PEDIDO.Where(d => d.ID_PEDIDO == model.IDPedido);
                    if (lst.Count() > 0 && lstDet.Count()>0)
                    {
                        oR.result = 1;
                        List<PedidoViewModel> pedidoN= lst.Select(pedido => new PedidoViewModel()
                        {
                          IDPedido=pedido.ID,
                          FechaPedido=(DateTime)pedido.DATE_SALE,
                          TotalPedido=(float)pedido.TOTAL,
                          UserName=pedido.USERNAME,
                          NombrePedido=pedido.NOMBREPEDIDO
                        
                        }).ToList();
                        pedidoN[0].DetallePedido = lstDet.Select(detPedido => new DetPedidoViewModel() 
                        {
                             SKU= detPedido.SKU_PRODUCTO,
                              Cantidad= detPedido.CANTIDAD,
                               PRECIO=detPedido.PRICE
                        }).ToList();
                        oR.data = pedidoN;

                    }
                    else
                    {
                        oR.message = "No se encuentran datos del pedido";
                    }
                }
            }
            catch (Exception ex)
            {
                oR.result = 0;
                oR.message = "Ocurrio un problema al obtener los productos intente mas tarde";
            }
            return oR;

        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public Reply EliminarPedido([FromBody]PedidoViewModel model)
        {
            Reply oR = new Reply();

            oR.result = 0;

            try
            {
                using (examenEntities db = new examenEntities())
                {
                    //var lst = db.PEDIDOS_W.Where(d => d.ID== pedidoID);
                    //var lstDet = db.DETALLE_PEDIDO.Where(d => d.ID_PEDIDO == pedidoID);
                    db.DETALLE_PEDIDO.RemoveRange(db.DETALLE_PEDIDO.Where(d => d.ID_PEDIDO == model.IDPedido));
                    db.PEDIDOS_W.RemoveRange(db.PEDIDOS_W.Where(d => d.ID == model.IDPedido));
                    db.SaveChanges();

                    oR.result = 1;
                    oR.message = "Se elimino satisfactoriamente el pedido:"+ model.IDPedido;

                }
            }
            catch (Exception ex)
            {
                oR.result = 0;
                oR.message = "Ocurrio un problema al eliminar el pedido intente mas tarde";
            }
            return oR;

        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public Reply GuardarPedido([FromBody]PedidoViewModel model)
        {
            Reply oR = new Reply();
            
            oR.result = 0;

            try
            {
                using (examenEntities db = new examenEntities())
                {
                    PEDIDOS_W oPedido = new PEDIDOS_W();
                    oPedido.DATE_SALE = DateTime.Now;
                    oPedido.USERNAME = model.UserName;
                    oPedido.NOMBREPEDIDO = model.NombrePedido;
                    double Total = 0;
                    foreach(DetPedidoViewModel elelment in model.DetallePedido)
                    {
                        DETALLE_PEDIDO oDetPedido = new DETALLE_PEDIDO();
                       
                        oDetPedido.ID_PEDIDO = oPedido.ID;
                        oDetPedido.SKU_PRODUCTO = elelment.SKU;
                        oDetPedido.CANTIDAD = elelment.Cantidad;
                        oDetPedido.PRICE = elelment.PRECIO;
                        Total += (elelment.Cantidad * elelment.PRECIO);
                        oPedido.DETALLE_PEDIDO.Add(oDetPedido);
                    }
                    oPedido.TOTAL = Total;
                    db.PEDIDOS_W.Add(oPedido);

                    db.SaveChanges();
                    oR.result = 1;
                    oR.message = "Guardado Con exito el Pedido:"+ oPedido.ID;

                }
            }
            catch (Exception ex)
            {
                oR.result = 0;
                oR.message = "Ocurrio un problema al guardar el pedido";
            }
            return oR;

        }
    }
}
