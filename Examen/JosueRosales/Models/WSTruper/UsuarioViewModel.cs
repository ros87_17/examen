﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JosueRosales.Models.WSTruper
{
    public class UsuarioViewModel
    {
        public string USERNAME { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Role { get; set; } 
    }
}