﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Examen_Josue_Rosales_Chimal.Models;
using JosueRosales.Models.WSTruper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Examen_Josue_Rosales_Chimal.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            if(Session["IsLoged"]==null || ((bool) Session["IsLoged"]) == false)
            {
                return Redirect(Url.Action("Login","Account"));
            }
            // Execute post method

            var httpClient = new HttpClient();
            try
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Session["TokenVal"].ToString());
                var json = await httpClient.GetStringAsync("http://localhost:8089/api/Pedido/ObtenerProductos");
                JObject jsonProductos = JObject.Parse(json);
                var listProd = JsonConvert.DeserializeObject<List<ProductoViewModel>>(jsonProductos["data"].ToString());
                return View(listProd);
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Guardarpedido(string[] pedido,string[] DetPedido) 
        {
            var httpClient = new HttpClient();
            PedidoViewModel objPedido = new PedidoViewModel();
            try
            {
                objPedido.NombrePedido = pedido[1];
                objPedido.UserName = pedido[0];
                List<DetPedidoViewModel> LobjDetP = new List<DetPedidoViewModel>();
                foreach (string item in DetPedido)
                {
                    DetPedidoViewModel itemDet = new DetPedidoViewModel();
                    itemDet.SKU = item.Split('_')[0];
                    int cant = 0;
                    int.TryParse(item.Split('_')[1], out cant);
                    itemDet.Cantidad = cant;
                    float precio = 0;
                    float.TryParse(item.Split('_')[2], out precio);
                    itemDet.PRECIO = precio;
                    LobjDetP.Add(itemDet);
                }
                objPedido.DetallePedido = LobjDetP;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Session["TokenVal"].ToString());

                var json = await httpClient.PostAsJsonAsync("http://localhost:8089/api/Pedido/GuardarPedido", objPedido);
                string bandera = "";
                if (json.IsSuccessStatusCode)
                { bandera = "Pedido Guardado"; }

                if (bandera != "")
                {

                    return Json(new { success = true, message = bandera });
                }
                else
                    return Json(new { success = false, message = "Hubo un error al Guardar el Pedido" });
            }
            catch (Exception ex) 
            { return Json(new { success = false, message = "Hubo un error al Guardar el Pedido" }); }
        }
    }
}