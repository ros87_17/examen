﻿using Examen_Josue_Rosales_Chimal.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Examen_Josue_Rosales_Chimal.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> Login(LoginModel model)
        {
           
            if (model == default(LoginModel))
                return Json(new { success = false, message = "los datos no son validos" });
            //var httpClient = new HttpClient();
            //var json = await httpClient.GetStringAsync("http://localhost:8089/api/Pedido/ObtenerProductos"); 
            var data = new
            {
                grant_type = "password",
                name = model.Usuario,
                category = model.Usuario
            };

            string bandera = "";
            var dict = new Dictionary<string, string>();
            dict.Add("grant_type", "password");
            dict.Add("username", model.Usuario);
            dict.Add("password", model.Password);
            var client = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, "http://localhost:8089/Token") { Content = new FormUrlEncodedContent(dict) };
            var res = await client.SendAsync(req);
            if (res.IsSuccessStatusCode) 
            {
                var respuestaF = await res.Content.ReadAsStringAsync();
                JObject jsonToken = JObject.Parse(respuestaF);
                string token = jsonToken["access_token"].ToString();
                Session["TokenVal"] =  jsonToken["access_token"];
                Session["UsrVal"] =  model.Usuario;
                bandera = "Exito";
            }

            // Add key/value


           
            //LogServ.Login(model.Usuario, model.Password);

            if (bandera!="")
            {
                Session["IsLoged"]=true;
                return Json(new { success = true, message =bandera }); }
            else
                return Json(new { success = false, message = "Usuario o contraseña no valido" });
        }
        public ActionResult Logout()
        {
            Session["IsLoged"] = null;
            return Redirect(Url.Action("Index", "Home"));
        }
    }
}