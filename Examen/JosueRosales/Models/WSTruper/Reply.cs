﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JosueRosales.Models.WSTruper
{
    public class Reply
    {
        public int result { get; set; }

        public object data { get; set; }

        public string message { get; set; }
    }
}