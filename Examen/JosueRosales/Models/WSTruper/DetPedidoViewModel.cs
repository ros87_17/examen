﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JosueRosales.Models.WSTruper
{
    public class DetPedidoViewModel
    {

        public string SKU { get; set; }
        public int Cantidad { get; set; }
        public double PRECIO { get; set; }
    }
}