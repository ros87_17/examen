﻿

using JosueRosales.Models.DB;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace JosueRosales.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
     

        
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // La credenciales de la contraseña del propietario del recurso no proporcionan un id. de cliente.
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Acces-Control-Allow-Origin", new[] { "*" });

           
                ClaimsIdentity identidad = new ClaimsIdentity(context.Options.AuthenticationType);
                identidad.AddClaim(new Claim(ClaimTypes.Name, context.Password));
                try
                {
                    using (examenEntities db = new examenEntities())
                    {
                        var lst = db.USUARIOS_W.Where(d => d.USERNAME == context.UserName && d.PASSWORD == context.Password);
                        if (lst.Count() > 0)
                        {

                            identidad.AddClaim(new Claim(ClaimTypes.Role, "ADMIN"));
                            context.Validated(identidad);
                           
                        }
                        else
                        {
                            context.SetError("Sin Acceso", "usuario o contraseña Equivocada");
                        }
                    }
                }
                catch (Exception ex)
                {
                    context.SetError("Ocurrio un error", "Intente mas tarde");
                }
            }
            
        

       
    }
}