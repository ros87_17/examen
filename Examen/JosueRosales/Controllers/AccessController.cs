﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JosueRosales.Models.WSTruper;
using JosueRosales.Models;
using JosueRosales.Models.DB;

namespace JosueRosales.Controllers
{
    public class AccessController : ApiController
    {
        [HttpGet]
        public Reply HelloWorld()
        {
            Reply oR = new Reply();
            oR.result = 1;
            oR.message = "Hola Mundo";
            return oR;
        }

        [HttpPost]
        public Reply Login([FromBody]AccesViewModel model)
        {
            Reply oR = new Reply();
            oR.result = 0;
            try
            {
                using (examenEntities db= new examenEntities())
                {
                    var lst = db.USUARIOS_W.Where(d => d.USERNAME == model.user && d.PASSWORD==model.password);
                    if (lst.Count() > 0)
                    {
                        oR.result = 1;
                        oR.data = Guid.NewGuid().ToString();
                        USUARIOS_W oUser = lst.First();
                       
                    }
                    else
                    {
                        oR.message = "Datos Incorrectos";
                    }
                }
            }
            catch (Exception ex)
            {
                oR.result = 0;
                oR.message = "Ocurrio un problema intente mas tarde";
            }
            return oR;
        }
    }
}
