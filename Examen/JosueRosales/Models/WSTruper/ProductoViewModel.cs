﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JosueRosales.Models.WSTruper
{
    public class ProductoViewModel
    {
        public string SKU { get; set; }
        public string Nombre { get; set; }
        public int Extistencia { get; set; }
        public float Price { get; set; }
    }
}